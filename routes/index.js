// const express = require('express');
// const router = express.Router();

//Same mais comme on n'aura plus besoin de express ensuite, peut se faire en une seule ligne 
const router = require('express').Router();

//Import router lvl middleware
const routerMiddleware = require('../middlewares/router.middleware')
//Sur toutes les routes auxquelles je l'ai ajouté, il sera déclenché
//route.method(url, middleware, () => {})

//Gestion des routes
//router.get('/', homeController.home_Get)
router.get('/', (req, res) => {
    console.log("HomePage GET");
    //On peut envoyer du text
    //res.end('Bonjour')
    //Un json sous forme de string
    let data = { id : 1, pseudo : 'Lhel', msg : 'Wesh Zalu'}
    //Toujours renvoyer un status :
    //Si données à envoyer en plus du status
    //res.status(200).send(JSON.stringify(data));
    //Si pas de données à envoyer en plus du status
    //res.sendStatus(404);
    
    //Un json
    res.status(200).json(data);
})
router.get('/contact', routerMiddleware , () => {
    console.log("Contact GET");
})
router.post('/contact', () => {
    console.log("Contact POST");
})
router.get('/about', () => {
    console.log("About GET");
    throw new Error("Kaboum 💣");
})
router.get('/fans', routerMiddleware , (req, res) => {
    // /fans est un segment statique
    console.log("Page affichant la liste de tous les fans");
})
//On peut "réguler" les paramètres que l'on souhaite grâce à des regex
router.get('/fans/:id([0-9]{1,5})', (req, res) => {
    //Pour récupérer l'id dans les paramètres et les réutiliser dans le code :
    // /fans est un segment statique
    // /:id est un segment dynamique
    const { id } = req.params;
    console.log(`Page affichant le fan dont l'id est : ${id}`);

    //#region  Parenthèse destructuring
    // const person = {
    //     idPerson : 1,
    //     firstname : 'Aude',
    //     lastname : 'Beurive'
    // }
    // const { idPerson, lastname } = person
    // console.log(lastname);
    //#endregion
})
//Si aucune des routes précédentes : (Attention, à TOUJOURS mettre en dernier, puisque reprend toutes les routes définies avant)
router.get('*', ( req, res) => {
    console.log("Route inconnue ! 404");
})

module.exports = router;